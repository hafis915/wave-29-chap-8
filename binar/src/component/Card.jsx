import React, {useEffect, useState} from "react"

export default function Card(props) {
    const [variabel, setVariabel] = useState('---')
    useEffect( () => {
        // setVariabel('++++++')
        console.log("==== card di tampilkan =====")
    } , [])
    return (
        <div style={{backgroundColor : 'yellow'}}  >
            <p>{variabel}</p>
            <p
            style={
                {
                    color : 'red',
                    fontSize : '12px'
                }
            }
            >Name : {props.data}</p>
            <p
                style={
                    {
                        backgroundColor : 'blue'
                    }
                }            
            >Address : {props.address} </p>
        </div>
    )
}