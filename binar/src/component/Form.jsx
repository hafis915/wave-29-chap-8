import React from "react"



export default function FormAddUser(props) {

    const variable = props.variable


    return (
        <div>
            <p className="red-change" >{props.type}</p>
            <input value={variable} onChange={props.handleChange} type="text" />
            <button onClick={props.handleSave} >Save</button>
        </div>
    )
}

