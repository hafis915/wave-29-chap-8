import React, {useEffect, useState} from "react"
import './table.css'

export default function TableUser({users, handleDelete}) {

    return (
        <div className="table-wrapper" >

            <div className="red-change" >aadadada</div>
            
            <table className="table-body">
                <thead>

                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    {   
                    //if (users) {
                        //users.map()
                    // }
                        users && users.map((el,index) => {
                            return (
                                <tr key={index} >
                                    <td>{el.id}</td>
                                    <td>{el.name}</td>
                                    <td
                                    style={{
                                        color : 'red',
                                        cursor: 'pointer'
                                    }}
                                    onClick={() => handleDelete(el.id)}
                                    >Delete</td>
                                </tr>
                            )
                        })
                    }

                </tbody>
            </table>
        </div>
    )
}