import React, {useState, useEffect} from "react";
import Card from "../component/Card";
import FormAddUser from "../component/Form";
import TableUser from "../component/Table/Table";
import axios from 'axios'
import './home.css'
import { Button } from "reactstrap";

export default function Home() {
    const [name, setName] = useState('')
    const [users, setUsers] = useState('')
    const [address, setAdress] = useState('')
    const [testVar, setTestVar] = useState('')
    // const [users, setUsers] = useState([])
    const [flagUpdateData, setFlagUpdateData] = useState(0)

    useEffect( () => {
        getAllUser()
       .then(res => {
           setUsers(res)
            // setTimeout(() => {
            // }, 2000)
        })
        .catch(err => {
            console.log(err)
        })
    }, [flagUpdateData])



    async function getAllUser() {
        try {
            let res = await axios.get('http://localhost:4000/user')
            return res.data.data
        } catch (error) {
            throw error
        }
    }

    async function deletUserAPI(id) {
        try {
            const res = await axios.delete(`http://localhost:4000/user/${id}`)

            return res.data
            
        } catch (error) {
            throw error
        }
    }

    async function saveNameAPI(payload) {
        try {
            /**
             * payload = { name : '' }
             */
            console.log(payload, "<< di save")
            const res = await axios.post('http://localhost:4000/user', payload)
            return res.data
        } catch (error) {
            throw error
        }
    } 


    async function handleDelete(id) {
        try {
            const deleteData = await deletUserAPI(id)
            // let user = await getAllUser()
            // setUsers(user)
            // const val = flagUpdateData + 1
            setFlagUpdateData(flagUpdateData+1)
            console.log(deleteData)
        } catch (error) {
            console.log(error)
        }
    }

    async function handleChange(e) {
        let val = e.target.value
        setName(val)
    }

    async function handleChangeAddress(e) {
        let val = e.target.value
        console.log(val)
        setAdress(val)
        // setName(val)
    }

    async function handleSave() {
        try {
            let payload = {
                name
            }
            let res = await saveNameAPI(payload)
            setFlagUpdateData(flagUpdateData + 1)
            setName('')
        } catch (error) {
            console.log(error)
        }
    }

    async function handleSaveAddress() {
        console.log(address, "<< ini address")
    }


    return(
        <div>
            <TableUser
                users={users}
                handleDelete={handleDelete}
            />
            <FormAddUser
                variable={name}
                handleChange={handleChange}
                handleSave={handleSave}
                type="Name"
            />

            <Button>TEST</Button>

            <FormAddUser 
                type="Address"   
                variable={address}
                handleChange={handleChangeAddress}
                handleSave={handleSaveAddress}

            
            />
        </div>
    )
} 
