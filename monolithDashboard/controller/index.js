const { news,user } = require('../models')

class Controller {
    static async getNews(req,res) {
        try {
            let data = await news.findAll()

            return res.status(200).json({
                data
            })
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async getUser(req,res) {
        try {
            let data = await user.findAll({
                order : [['id', 'asc']]
            })
            return res.status(200).json({
                data
            })
            
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }
    static async createNews(req,res) {
        try {
            let { title, desc, body , userId} = req.body
            let params = {
                title,desc,body,
                isReleased : false,
                userId
            }

            let data = await news.create(params)
            return res.status(200).json({
                data
            })

        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async createUser(req,res) {
        try {
            let { name } = req.body
            console.log(name)
            let params = {
                name, 
                joinDate : new Date()
            }

            let data = await user.create(params)
            return res.status(200).json({
                data
            })
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async deleteNews(req,res) {
        try {
            let { id } = req.params
            let data = await news.destroy({
                where : {
                    id
                }
            })
            console.log(data)
            return res.status(200).json({
                data
            })
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async deleteUser(req,res) {
        try {
            let { id }  = req.params
            console.log(id)
            let data = await user.destroy({
                where : {
                    id
                }
            })
            return res.status(200).json({
                data
            })
            
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async homeView(req,res) {
        try {
            let users = await user.findAndCountAll()
            let _news = await news.findAndCountAll()

            // _news = _news.toJSON()
            // console.log(_news)
            let data = {
                users : users.count,
                news : _news.count,
                newsBody : _news.rows
            }


            res.render('./index', data)
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }

    static async loginView(req,res) {
        try {
            res.render('./login')
        } catch (error) {
            return res.status(400).json({
                error : error.message || error
            })
        }
    }
}



module.exports = Controller
