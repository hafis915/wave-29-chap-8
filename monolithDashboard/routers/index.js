const router  = require('express').Router()
const Controller  = require('../controller')
const { passport } = require('../helpers')

const { authent, author } = require('../middleware')

router.post('/login', passport.authenticate('local', {
    successRedirect : '/views',
    failureRedirect : '/views/login'
}))

// router.post('/login', (req,res) => {
//     console.log(req.body)
// })
router.get('/news',Controller.getNews )
router.post('/news', authent,Controller.createNews)
router.delete('/news/:id', authent,Controller.deleteNews)

router.get('/user', Controller.getUser)
router.post('/user', Controller.createUser)
router.delete('/user/:id', Controller.deleteUser)

router.get('/views', authent, Controller.homeView)
router.get('/views/login', Controller.loginView)


module.exports = router