'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      this.hasMany(models.news, {foreignKey : 'userId'})
    }
  }
  user.init({
    name: DataTypes.TEXT,
    joinDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};