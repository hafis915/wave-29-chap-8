/**
 * @swagger
 * /users:
 *  get: 
 *      summary: This is summary of swagger get method
 *      description: Desc    
 *      responses: 
 *          200:
 *              description : A list of users.
 *              content: 
 *                  application/json:
 *                      schema: 
 *                          type : object
 *                          properties: 
 *                              data:
 *                                  type: array
 *                                  items: 
 *                                      type: object
 *                                      properties: 
 *                                          id : 
 *                                              type: integer
 *                                              description: User desc
 *                                              example: 0
 *                                          name : 
 *                                               type :string
 *                                               description: desc
 *                                               example : hafis
 */

const router = require('express').Router()


router.get('/', (req,res) => {
    res.send('/get')
})

module.exports =router