const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express');
const express = require('express')
var app = express();

// const swaggerDefinition = {
//     openapi : '3.0.0',
//     info: {
//         title: 'Wave 29 SWAGGER',
//         version: '1.0.0',
//     },
//     servers: [
//         {
//           url: 'http://localhost:3000',
//           description: 'Development server',
//         },
//     ],
// }

// const options = {
//     swaggerDefinition,
//     // Paths to files containing OpenAPI definitions
//     apis: ['./routes/*.js'],
// };

// const swaggerSpec = swaggerJSDoc(options); 
const apiDocs = require('./apiDocs.json')
app.use('/docs', swaggerUi.serve, swaggerUi.setup(apiDocs));


app.listen(3000, () => {
    console.log('start')
})